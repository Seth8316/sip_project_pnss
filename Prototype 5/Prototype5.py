#Copyright (c) 2020, Juan C Rivera
#All rights reserved.

#This source code is licensed under the BSD-style license found in the
#LICENSE file in the root directory of this source tree. 


from tkinter import *
from tkinter.ttk import *  # Importing needed modules
import webbrowser
import os
from tkinter import messagebox
import GWaddress
import Macaddressgrabber
import tkinter.simpledialog
import string
import secrets

master = Tk()      # Setting up the window for the program
master.title('Parrot Network Security')
master.geometry("600x600")


def UCVE():  # Functions that open links
    webbrowser.open_new_tab("https://cve.mitre.org/data/downloads/index.html")


def UMDB():
    webbrowser.open_new_tab("https://macaddress.io/database-download")


def VIN():
    webbrowser.open_new_tab("https://cwe.mitre.org/top25/archive/2020/2020_cwe_top25.html")


def NMAP():
    webbrowser.open_new_tab("https://nmap.org/")


def WS():
    webbrowser.open_new_tab("https://www.wireshark.org/")


def WSI():
    webbrowser.open_new_tab("https://www.wireshark.org/docs/wsug_html_chunked/index.html")


def msg():  # Functions to sift through data
    with open('Detected Router Manufactorer info.txt ', 'r') as routerI:
        RI = routerI.read()
        showMsg(RI)

            
def Ptext():  # Function to save password obtained from secret function and write to file
    with open('Users-Pwds.txt', 'r') as password:
        pass2 = password.readlines()
        pass1 = (pass2[-1])
        showMsg2(pass1)


def testr():
    exec(open('test.py').read())
    with open("testM.txt", 'r') as test3:
        data1 = test3.read()


def testr2(data1):
    showMsg4(data1)


def GWG():  # Function to open the Command line output and grab needed info
    with open("CMDoutput2.txt", 'r') as GWI:
        dataT2 = GWI.readlines()
        data2 = (dataT2[3])
        data2a = data2[:15]
        print(data2a)
        showMsg4(data2a)


def vd():  # Executes script Database crawler
    os.system('python DatabaseCrawler.py')


def learnmore():  # Loads messages for user and defines info messages
    linfo1 = "There will always be more to learn on the subject of both utilizing your computer and internet for buisness/personal needs.\n    For more information on how to set up a database sites like MySQL, CouchDB are good starters both involve port fowarding as well and a bit of tinkering within the files it includes.\n     For more those willing to learn more about networking learning the basics first and seeing them in action tools like network miner or weireshark are really good to start with.\n    Fully understanding how to foward and how it is used is also a very improtant step as it is used in a variety of networking tools and methods."
    infoMsg6(linfo1)


def nextsteps(): 
    info1 = "   Now that you've detected your router model, found out how to log in to it and found any possible vulnerabilites in it it is time to take the next step.\n    If there is a vulnerability for your router model it is best to follow best practices as well as look more into the detected CVE that could associated with the router the best way to go about this is simply googling the CVE number and router model."
    info2 = "\n   Always ensure you have a strong password for both network and router login, a password generator is included in this program.\n   Keep your firmware up to date.\n   Leave any unnecessary ports closed.\n   Keep an antivirus and firewall on at all times and do not visit any malicious websites."
    infoMsg5(info1, info2)


def wheretogo():
    winfo1 = "If you wish to learn more about the tools used for network security there are a lot of options on windows.\n       Wireshark \n       Network miner \n       Nmap \n       Suricata"
    winfo2 = "\n   All of these tools gives a fundamental understanding of how analyzing network traffic is, this information is meant for those who take an interest in network security. There are hundreds of ways to learn how to utilize these tools online, most guides and lessons can be found on the website themselves, some Linux distribution operating systems like Kali Linux are also available freely to those who wish to have a more hands on experience. Always remember to safely and legally experiment with tools on both platforms, websites like http://scanme.nmap.org/ exist for this reason."
    infoMsg7(winfo1, winfo2)


def showMsg(RI):  # These functions defines the window name that the button opens and shows displays any info associated with the window. Other actions include defining the action a button will take such as opening a webbrowser.
    messagebox.showinfo("Router information", "This information contains the mac adress, company name and address registered to the mac address:   " + RI)


def showMsg2(pass1):
    messagebox.showinfo("Generated Password", pass1)


def showMsg3():
    messagebox.showinfo("Password information", "The password button will generate a 10-32 character password with uppercase,lowercase, and numbers the password will also be saved to a text file in the current working directory named User-Pwds")


def showMsg4(data2a):
    global data0
    data0 = ""
    data0 = data2a
    messagebox.showinfo("Finding Router Model", "To find your router model enter your gateway address " + data2a + "into your browser searchbar, you may need your router login information, if this was never changed please consult your router manual. ")


def showMsg5():
    messagebox.showinfo("Detect Possible Router Vulnerabilities")


def connect(data0):
    webbrowser.open_new_tab(data0)


def routerlogin():
    connect(data0)


def infoMsg5(info1, info2):
    messagebox.showinfo("What to do next", info1 + info2)


def infoMsg6(linfo1):
    messagebox.showinfo("Learn more", linfo1)


def infoMsg7(winfo1, winfo2):
    messagebox.showinfo("Where to learn more about cyber security", winfo1 + winfo2)


def ask():
    ask2(var)


def ask2():  # This functions opens a ask dialog window and them compares the user entry to a csv database. The results are saved to a textfile. 
    var = tkinter.simpledialog.askstring("Router/Model", "Router model or brand")

    with open('Router3.txt', 'w') as routerI4:
                routerI4.write(var)
    with open('Router3.txt', 'r') as routerI5:
        var1 = routerI5.read()
        print(var1)

    with open('CVE Search Results.txt', 'w') as routerI2:
        routerI2.write('')

    with open('allitems.csv', encoding='utf-8', errors='ignore') as VDB:
        found = False
        for line in VDB:
            if var1 in line:
                print(line)
                found = True
                with open('CVE Search Results.txt', 'a') as routerI2:
                    routerI2.write('    ' + line)


def end(pass1): # This function appends the generated password from seceret to a text file and keeps them seperated by a comma followed by a new line. 
    Ptext = open('Users-Pwds.txt', 'a+')
    Ptext.write('\n')
    Ptext.write(pass1)
    Ptext.write(",")
    Ptext.close
    showMsg2(pass1)


def Pgen(): # This is the secret function generating a 32 character function with Uppercase, lowercase and numbers. 
    alphabet = string.ascii_letters + string.digits
    while True:
        pass1 = ''.join(secrets.choice(alphabet) for i in range(32))
        if (any(c.islower() for c in pass1)
                and any(c.isupper() for c in pass1)
                and sum(c.isdigit() for c in pass1) >= 10):
            end(pass1)
            break


def NewWindow1(): # These functions define the windows and ties functions to button presses.
        newWindow1 = Toplevel(master)
        newWindow1.title("Resources")
        newWindow1.geometry("300x200")

        buttonRL1 = Button(newWindow1,
                           text='Vulnerability database',
                           command=UCVE)
        buttonRL1.pack(side=LEFT)
        buttonRL2 = Button(newWindow1,
                           text='MacAddress Database',
                           command=UMDB)
        buttonRL2.pack(side=RIGHT)


def NewWindow2():
    newWindow2 = Toplevel(master)
    newWindow2.title("Main commands")
    newWindow2.geometry("550x300")
    button1 = Button(newWindow2,
                     text='Show detected Router Information',
                     command=msg)
    button1.pack()
    button2 = Button(newWindow2,
                     text='Generate a strong password',
                     command=Pgen)
    button2.pack()
    button4 = Button(newWindow2,
                     text="Router model",
                     command=GWG)
    button4.pack()
    button5 = Button(newWindow2,
                     text="Vulnerability Check",
                     command=ask2)
    button5.pack()
    buttonGWA = Button(newWindow2,
                       text='Router page',
                       command=routerlogin)
    button3 = Button(newWindow2,
                     text="Password information",
                     command=showMsg3)
    button3.pack()


def NewWindow3():
    newWindow3 = Toplevel(master)
    newWindow3.title("More information")
    newWindow3.geometry("400x400")
    buttonIN1 = Button(newWindow3,
                       text="Learn about vulnerabilities",
                       command=VIN)
    buttonIN1.pack()
    Label(newWindow3,
          text="More useful Networking tools below").pack()
    buttonNMAP = Button(newWindow3,
                        text="Nmap homepage",
                        command=NMAP)
    buttonNMAP.pack()
    buttonWS = Button(newWindow3,
                      text="Wireshark",
                      command=WS)
    buttonWS.pack()
    buttonWSI = Button(newWindow3,
                       text="How to use Wireshark",
                       command=WSI)
    buttonWSI.pack()
    button6 = Button(newWindow3,
                     text="Next step",
                     command=nextsteps)
    button6.pack()

    button7 = Button(newWindow3,
                     text="Learn more",
                     command=learnmore)
    button7.pack()

    button8 = Button(newWindow3,
                     text="Where to learn more",
                     command=wheretogo)
    button8.pack()
greeting = Label(text="This is very very basic prototype of PNSS!")
greeting2 = Label(text="This is an Early version showing of its major functionalites!")
information = Label(text="Using multiple modules like TKinter, OS, and csv I was able to create button to automatically grab the router model and show it to the user!")
information2 = Label(text="The program will show the identifying router information, vendor, and its registration address.")
greeting.pack()
greeting2.pack()
information.pack()
information2.pack()
# Setting up the buttons as well as tying it to the functions it will call

buttonB1 = Button(master,  # These buttons call the new windows to keep things are clean
                  text='Resources',
                  command=NewWindow1)
buttonB1.pack()
buttonB1.pack(pady=10)

buttonB2 = Button(master,
                  text='Main Commands',
                  command=NewWindow2)
buttonB2.pack()
buttonB2.pack(pady=15)

buttonI1 = Button(master,
                  text='More information',
                  command=NewWindow3)
buttonI1.pack()
master.mainloop()
