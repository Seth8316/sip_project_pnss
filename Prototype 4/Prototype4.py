import tkinter as PNSS                          #Importing needed modules
import test
import PasswordGenerator
import Macaddressgrabber
import GW
import os
import test3
import test4
from tkinter import messagebox

def msg():
    with open('Router.txt', 'r') as routerI:
        RI = routerI.read()
        print(RI)
        showMsg(RI)
def Ptext():
    with open('Users-Pwds.txt', 'r') as password:
        pass2 = password.readlines()
        pass1 = (pass2[-1])

        showMsg2(pass1)
def testr():
    exec(open('test.py').read())
    with open("testM.txt", 'r') as test3:
        data1=test3.read()
        
def testr2(data1):
    
            showMsg4(data1)
    
def GWG():
    with open("CMDoutput2.txt", 'r') as GWI:
        dataT2 = GWI.readlines()
        data2 = (dataT2[-1])
        print (data2)
        showMsg4(data2)
def vd():
    os.system('DatabaseCrawler.py')
def learnmore():
     linfo1 = "There will always be more to learn on the subject of both utilizing your computer and internet for buisness/personal needs.\n    For more information on how to set up a database sites like MySQL, CouchDB are good starters both involve port fowarding as well and a bit of tinkering within the files it includes.\n     For more those willing to learn more about networking learning the basics first and seeing them in action tools like network miner or weireshark are really good to start with.\n    Fully understanding how to foward and how it is used is also a very improtant step as it is used in a variety of networking tools and methods."
     infoMsg6(linfo1)    
def nextsteps():
    info1 = "   Now that you've detected your router model, found out how to log in to it and found any possible vulnerabilites in it it is time to take the next step.\n    If there is a vulnerability for your router model it is best to follow best practices as well as look more into the detected CVE that could associated with the rotuer the best way to go about this is simply googling the CVE number and router model."
    info2 = "\n   Always ensure you have a strong password for both network and router login, a password generator is included in this program.\n   Keep your firmware up to date.\n   Leave any unnecessary ports closed.\n   Keep an antivirus and firewall on at all times and do not visit any malicous websites."  
    infoMsg5(info1, info2)    
window = PNSS.Tk()      #Setting up the window for the program
window.title('Parrot Network Security')
greeting = PNSS.Label(text="This is very very basic prototype of PNSS!")
greeting2= PNSS.Label(text="This is an Early version showing of its major functionalites!")
information=PNSS.Label(text="Using multiple modules like TKinter, OS, and csv I was able to create button to automatically grab the router model and show it to the user!")
information2=PNSS.Label(text="The program will show the identifying router information, vendor, and its registration address.")
greeting.pack()
greeting2.pack()
information.pack()
information2.pack()
                            #setting up the buttons as well as tying it to the functions it will call 
def showMsg(RI):  
    messagebox.showinfo("Router information","This information contains the mac adress, company name and address registered to the mac address:   " + RI)
button1 = PNSS.Button(window,
	text = 'Show detected Router Information',
	command = msg)  
button1.pack(side= PNSS.LEFT, padx=10)
def showMsg2(pass1):
    messagebox.showinfo("Generated Password", pass1)
button2 = PNSS.Button(window,
                      text = 'Generate a strong password',
                      command = Ptext)
button2.pack(side= PNSS.LEFT, pady=30)
def showMsg3():
    messagebox.showinfo("Password information", "The password button will generate a 10-32 character password with uppercase,lowercase, numbers and symbols the password will also be saved to a text file in the current working directory named User-Pwds")
button3 =  PNSS.Button(window,
                       text = "Password information",
                       command = showMsg3)
button3.pack(side= PNSS.RIGHT)
def showMsg4(data2):
    messagebox.showinfo("Finding Router Model","To find your router model enter your gateway address " + data2 + "into your browser searchbar, you may need your router login information, if this was never changed please consult your router manual.  ")

button4 = PNSS.Button(window,
                      text= "Router model",
                      command = GWG)
button4.pack(side = PNSS.RIGHT)
def showMsg5():
    messagebox.showinfo("Detect Possible Router Vulnerabilities")
button5 = PNSS.Button(window,
                      text= "Vulnerability",
                      command = vd)
button5.pack()
def infoMsg5(info1, info2):
    messagebox.showinfo("What to do next", info1 +info2)
button6 = PNSS.Button(window,
                       text= "Next step",
                       command = nextsteps)
button6.pack()
 
def infoMsg6(linfo1):
    messagebox.showinfo("Learn more", linfo1)
button7 = PNSS.Button(window,
                      text = "Learn more",
                      command = learnmore)
button7.pack()
                        
